import React, { useState } from "react";
import { ToastContainer, toast } from "react-toastify";
import "react-toastify/dist/ReactToastify.min.css";

const InputForm = () => {
  const [url, setUrl] = useState("");
  const [email, setEmail] = useState("");
  const [details, setDetails] = useState("");

  const myHeaders = new Headers();
  myHeaders.append("Content-Type", "application/json");

  const handleSubmit = async (e) => {
    e.preventDefault();
    const raw = JSON.stringify({
      url,
      email,
      details,
    });

    setDetails(" ");
    setEmail(" ");
    setUrl(" ");

    const requestOptions = {
      method: "POST",
      headers: myHeaders,
      body: raw,
      redirect: "follow",
    };

    toast.promise(fetch("/api/data", requestOptions), {
      pending: "Request is pending",
      success: "Request Complete 👌",
      error: "Request Fail 🤯",
    });
  };

  return (
    <div>
      <form
        onSubmit={(e) => {
          handleSubmit(e);
        }}
      >
        <div className="mb-3">
          <label className="form-label">URL</label>
          <input
            type="url"
            className="form-control"
            id="InputUrl"
            autoFocus
            aria-describedby="url-describedby"
            onChange={(e) => setUrl(e.target.value)}
            required
          />
          <div id="url-describedby" className="form-text">
            Enter you URL Here!
          </div>
        </div>
        <div className="mb-3">
          <label className="form-label">Email</label>
          <input
            type="email"
            className="form-control"
            id="InputEmail"
            aria-describedby="email-describedby"
            onChange={(e) => setEmail(e.target.value)}
          />
          <div id="email-describedby" className="form-text">
            Enter your Email Here!
          </div>
        </div>
        <div className="mb-3">
          <label htmlFor="detailsTextArea">Details</label>
          <textarea
            className="form-control"
            id="detailsTextArea"
            aria-describedby="details-describedby"
            onChange={(e) => setDetails(e.target.value)}
            style={{ height: 226 }}
          ></textarea>
          <div id="details-describedby" className="form-text">
            Enter your Details Here!
          </div>
        </div>
        <div className="d-grid gap-2 col-3 mx-auto">
          <button className="btn btn-primary" type="submit">
            Submit
          </button>
        </div>
      </form>
      <ToastContainer
        position="bottom-right"
        autoClose={5000}
        hideProgressBar={false}
        newestOnTop
        closeOnClick
        rtl={false}
        pauseOnFocusLoss
        draggable
        pauseOnHover
      />
    </div>
  );
};

export default InputForm;
