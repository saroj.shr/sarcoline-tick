import { Navbar, Container, Nav, NavDropdown, Image } from "react-bootstrap";

const NavBar = () => {
  return (
    <div>
      <Navbar expand="lg">
      <hr/>
        <Container>
          <Navbar.Brand href="#home">
            <Image
              src="/bug.svg"
              alt=""
              width="30"
              height="30"
              classNav="d-inline-block align-text-top"
            />
            Sarcoline Tick
          </Navbar.Brand>
        </Container>
      </Navbar>
    </div>
  );
};

export default NavBar;
