import { Image } from "react-bootstrap";

export const Hero = () => {
  return (
    <>
      <h1 className="visually-hidden">Sarcoline Tick</h1>

      <div className="px-4 py-3 my-3 text-center">
        <Image
          className="d-block mx-auto mb-4"
        src="/bug.svg"
          alt=""
          width="128"
          height="128"
        />
        <h1 className="display-5 fw-bold">Sarcoline Tick</h1>
        {/* <div className="col-lg-6 mx-auto">
          <p className="lead mb-4">
            Simple Project that just gets URL, Email and Details.
            &quot;Sarcoline Tick&quot; I don&apos;t. Got it from random name
          </p>
        </div> */}
      </div>
    </>
  );
};
