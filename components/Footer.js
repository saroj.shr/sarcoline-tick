import Link from "next/link";
import { Image } from "react-bootstrap";

export const Footer = () => {
  return (
    <>
      <footer className="d-flex flex-wrap justify-content-between align-items-center py-3 my-4 border-top">
        <div className="col-md-4 d-flex align-items-center">
          <span className="text-muted">Sarcoline Tick</span>
        </div>

        <ul className="nav col-md-4 justify-content-end list-unstyled d-flex">
          <li className="ms-3">
            <Link href="https://gitlab.com/saroj.shr/sarcoline-tick">
              <a className="text-muted">
                <Image
                  className="d-block mx-auto mb-2"
                  src="/gitlab.svg"
                  alt=""
                  width="24"
                  height="24"
                />
              </a>
            </Link>
          </li>
        </ul>
      </footer>
    </>
  );
};
