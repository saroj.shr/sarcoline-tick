import { google } from "googleapis";

const spreadsheetId = process.env.SHEET_ID;
const base64 = Buffer.from(
  process.env.GOOGLE_APPLICATION_CREDENTIAL,
  "base64"
).toString("ascii");
const timeZone =
  process.env.TIMEZONE.length > 0 ? process.env.TIMEZONE : "en-us";

export default async function handler(req, res) {
  const {
    type,
    project_id,
    private_key_id,
    private_key,
    client_email,
    client_id,
    auth_uri,
    token_uri,
    auth_provider_x509_cert_url,
    client_x509_cert_url,
  } = JSON.parse(base64);

  const auth = new google.auth.GoogleAuth({
    credentials: {
      type,
      project_id,
      private_key_id,
      private_key,
      client_email,
      client_id,
      auth_uri,
      token_uri,
      auth_provider_x509_cert_url,
      client_x509_cert_url,
    },
    scopes: "https://www.googleapis.com/auth/spreadsheets",
  });

  const client = await auth.getClient();
  const googleSheets = google.sheets({
    version: "v4",
    auth: client,
  });

  if (req.method === "POST") {
    const timeStamp = new Date().getTime();
    const localTime = new Date(timeStamp).toLocaleString(timeZone);
    try {
      await googleSheets.spreadsheets.values.append({
        auth,
        spreadsheetId,
        range: "Sheet1!A:B",
        valueInputOption: "USER_ENTERED",
        resource: {
          values: [
            [
              timeStamp,
              localTime,
              req.body.url,
              req.body.email,
              req.body.details,
            ],
          ],
        },
      });
      res.status(200).json(req.body);
    } catch (error) {
      console.log(error);
      console.error("Issue while connecting google sheet!");
      res.status(500).json({
        message: "Internal Error!",
      });
    }
  }
}
