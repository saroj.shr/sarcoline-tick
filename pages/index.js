import Head from "next/head";
import { Container, Row, Col } from "react-bootstrap";
import { Footer } from "../components/Footer";
import { Hero } from "../components/Hero";
import InputForm from "../components/InputForm";

export default function Home() {
  return (
    <>
      <Head>
        <title>Sarcoline Tick</title>
        <meta name="description" content="Sarcoline Tick" />
        <link rel="icon" href="/bug.svg" />
      </Head>

      <Hero />

      <Container>
        <Row>
          <Container>
            <Row className="justify-content-md-center">
              <Col sm="12" md="2" />
              <Col sm="12" md="8">
                <InputForm />
              </Col>
              <Col sm="12" md="2" />
            </Row>
          </Container>
        </Row>
        <Footer className="my-5 pt-5 text-muted text-center text-small" />
      </Container>
    </>
  );
}
